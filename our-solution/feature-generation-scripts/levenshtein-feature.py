from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import sys


data_file=sys.argv[1]
stopset = ['the', 'in', 'a', 'is', 'are', 'an']

def levenshtein(a,b):
    "Calculates the Levenshtein distance between a and b."
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a,b = b,a
        n,m = m,n

    current = range(n+1)
    for i in range(1,m+1):
        previous, current = current, [i]+[0]*n
        for j in range(1,n+1):
            add, delete = previous[j]+1, current[j-1]+1
            change = previous[j-1]
            if a[j-1] != b[i-1]:
                change = change + 1
            current[j] = min(add, delete, change)

    return 1 - (float(current[n])/max(len(a), len(b)))

lines=open(data_file).readlines()
lineno=1
with open("levenshtein-feature", "a") as f:
	for line in lines:
		line=line.lower()
		lineno=lineno+1
		question=line.split('|')[7]
		answer=line.split('|')[13]

		question_tokens=[qtoken for qtoken in word_tokenize(question) if qtoken.lower() not in stopset]
		answer_tokens=[atoken for atoken in word_tokenize(answer) if atoken.lower() not in stopset]

		similarities = []
		for qtoken in question_tokens:
			sim=[]
			for atoken in answer_tokens:
				sim.append(levenshtein(qtoken, atoken))
			if len(sim) ==0 :#some questions do not have answer body
        	                sim.append(0)
			similarities.append(max(sim))

		sim1 = sum(similarities)/len(question_tokens)

		similarities = []
	       	for atoken in answer_tokens:
        	       	sim=[]
               		for qtoken in question_tokens:
                       		sim.append(levenshtein(atoken, qtoken))
			if len(sim) ==0 :#some questions do not have answer body
				sim.append(0)
                	similarities.append(max(sim))

		if len(answer_tokens)==0:
			sim2=0
		else:
			sim2 = sum(similarities)/len(answer_tokens)
		similarity=(sim1+sim2)/2

		f.write(str(similarity)+'\n')
f.close()
