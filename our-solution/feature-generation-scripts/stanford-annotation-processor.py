from os import listdir
from os.path import isfile, join

files = [ f for f in listdir('stanford-annotations/') if isfile(join('stanford-annotations/',f)) ]

buffer=''
annotations=['ORGANIZATION','LOCATION','TIME','PERSON','MONEY', 'PERCENT','DATE']

for file in files:
	content=''
	for annotation in annotations:
		for line in open('stanford-annotations/'+file).readlines():
			components=line.split()
			matching_words = [x for x in line.split() if x.endswith(annotation)]
			previous_index=-1
			current_token=''
			for word in matching_words:
				buffer+=word.split('/')[0]+' '
	
				this_index=components.index(word)
				if len(components)>this_index+1:
					if components[this_index+1] not in matching_words:
						buffer+='\t'+annotation
	                			content+=buffer+'\n'
						buffer=''
				else:
					buffer+='\t'+annotation
                		        content+=buffer+'\n'
                        		buffer=''
	
				previous_index=components.index(word)
	f = open('stanford-annotations-processed/'+file,'w')
	f.write(content)
	f.close()

