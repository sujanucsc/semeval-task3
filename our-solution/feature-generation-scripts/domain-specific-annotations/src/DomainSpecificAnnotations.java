import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DomainSpecificAnnotations {

	static String[] domainSpecificLocations = {" ql ", "qatarliving"};
	
	public static void main(String[] args) throws IOException {
		String str = "(?i)\\b((?:https?://|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:\'\".,<>?«»“”‘’]))";
		Pattern patt = Pattern.compile(str);
		
		Scanner scan = new Scanner(new File("../../train-data-set"));
		
		while(scan.hasNextLine()){
			StringBuffer buf = new StringBuffer();
			String[] components = scan.nextLine().split("\\|");
			String comment = components[13].toLowerCase();
			Matcher matcher = patt.matcher(comment);
			
			for(String domainSpecificLocation : domainSpecificLocations){
				if (comment.contains(domainSpecificLocation)){
					buf.append(domainSpecificLocation+"\t"+"LOCATION\n");
				}
			}
			
			while(matcher.find()) {
				String urlStr = matcher.group();
				if(!(urlStr.endsWith(".gif") || urlStr.endsWith(".jpg") || urlStr.endsWith(".jpeg"))){
					buf.append(urlStr+"\t"+"LOCATION\n");
				}
			}
			writeToFile("../domain-specific-annotations-processed/"+components[8]+".txt", buf.toString());
		}
	}
	
	public static void writeToFile(String fileName, String content) throws IOException{
		FileWriter fw = new FileWriter(fileName);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(content);
		bw.close();
	}

}
