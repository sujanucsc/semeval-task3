#Process the AnswerPairwiseSimilarity to generate two files mean-pairwise-similarity-good and mean-pairwise-similarity-bad
#which contains filename and mean similarity
import os

def printorderedsimilarity(goodfilessimilarity, badfilesimilarity):
	printline = ""
	dGreaterthan = 0
	dLessthan = 0
	for i in range(0, 2600):
		qid = i + 1
		questionid = 'Q'+str(qid)
		goodaveragescore = 0
		badaveragescore = 0
		if questionid in goodfilessimilarity:
			goodaveragescore = goodfilessimilarity.get(questionid)
		if questionid in badfilesimilarity:
			badaveragescore = badfilesimilarity.get(questionid)
		printline = printline + questionid+"\t"+str(goodaveragescore) + "\t"+str(badaveragescore) + "\n"
		if goodaveragescore > badaveragescore:
			dGreaterthan = dGreaterthan + 1
		elif goodaveragescore <= badaveragescore:
			dLessthan = dLessthan + 1
		
	print 'Similarity is higher for good cases '+str(dGreaterthan)	
	print 'Similarity is lesser or equal good cases '+str(dLessthan)
	file = open('../TypeSimilarityByClass', 'w+')
	file.write(printline)


		

goodfilesimilarity = {}
badfilesimilarity={}

for line in open("../AnswerPairwiseSimilarity/PairWiseSimilarities"):
	lines = line.strip().split('\t')
	filename = lines[0].strip()
	similarity = lines[1].strip()
	classlabel = filename.split('-') 
	if classlabel[1].strip() == 'Good':
		goodfilesimilarity[classlabel[0].strip()] = similarity
	elif classlabel[1].strip() == 'Bad':
		badfilesimilarity[classlabel[0].strip()] = similarity

print 'size of good file similarities '+str(len(goodfilesimilarity))
print 'size of bad file similarities '+str(len(badfilesimilarity))
printorderedsimilarity(goodfilesimilarity, badfilesimilarity)

