import os
import sys

def printmaps(filename, dictelement):
	dictitmes = dictelement.items()
	printline = ""
	for dictItem in dictitmes:
		printline = printline + dictItem[0]+"\t"+str(dictItem[1]) + "\n"		
	#print printline	
	file = open(filename, 'w+')
	file.write(printline)


#print 'Number of arguments:', len(sys.argv)
if(len(sys.argv) != 3):
	print 'Please use the command line args for parameters'
	print 'python annotation-statgen.py <annotationtool> <annotationfolder>'
	sys.exit(2)

#annotation = "dbpedia"
#annotationfolder = "dbpediaannotationsprocessed"
annotation = str(sys.argv[1])
annotationfolder = str(sys.argv[2])


docannotationcount = {}
urifreqcount = {}
uridocufreqcount = {}
for file in os.listdir("../"+annotationfolder):
	#print file
	lstoffileurls = list()
	for line in open("../"+annotationfolder+"/"+file):	
		labelstring = line.split('\t')
		url = labelstring[1].strip()
		lstoffileurls.append(url)
		if url in urifreqcount:
			frqcount = urifreqcount.get(url)
			urifreqcount[url] = frqcount + 1
		else:
			urifreqcount[url] = 1

	setoffileurls = set(lstoffileurls)
	docannotationcount[file] = len(setoffileurls)
	for uniqueurls in setoffileurls:
		if uniqueurls in uridocufreqcount:
			doccount = uridocufreqcount.get(uniqueurls)
			uridocufreqcount[uniqueurls] = doccount + 1
		else:
			uridocufreqcount[uniqueurls] = 1

statFoldername = "../AnnotationStats"
printmaps(statFoldername+"/"+annotation+"_annotperdoc",docannotationcount)
printmaps(statFoldername+"/"+annotation+"_docperurl",uridocufreqcount)
printmaps(statFoldername+"/"+annotation+"_freperurl",urifreqcount)
