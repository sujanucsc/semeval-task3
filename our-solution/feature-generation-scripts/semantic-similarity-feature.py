from __future__ import division
from nltk.corpus import wordnet as wn
from nltk.corpus import wordnet_ic
from nltk.tokenize import word_tokenize
import nltk
from sets import Set
import sys


def levenshtein(a,b):
    "Calculates the Levenshtein distance between a and b."
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a,b = b,a
        n,m = m,n

    current = range(n+1)
    for i in range(1,m+1):
        previous, current = current, [i]+[0]*n
        for j in range(1,n+1):
            add, delete = previous[j]+1, current[j-1]+1
            change = previous[j-1]
            if a[j-1] != b[i-1]:
                change = change + 1
            current[j] = min(add, delete, change)

    return current[n]

def getWordForm(word):
        synset = wn.synsets(word)
        resultLemmas = []
        result = []
	flagNoun = 0
	flagVerb = 0
        for syn in synset:
                for lemma in syn.lemmas:
			if((wn.morphy(word) == lemma.name or lemma.name == word) and lemma.synset.pos == 'n'): #if given word is noun or verb we dont need to do anything
				result.append(word+".n")
				return result
			elif(lemma.synset.pos == 'v'):
				resultLemmas.append(lemma.derivationally_related_forms())
			elif(lemma.name == word and (lemma.synset.pos == 'a' or lemma.synset.pos == 's')):
				resultLemmas.append(lemma.derivationally_related_forms())
			elif(lemma.name == word and lemma.synset.pos == 'r'):
				resultLemmas.append(adverbLemma for adverbLemmaList in getNounForAdverb(lemma) for adverbLemma in adverbLemmaList)

	for lemmas in resultLemmas:
                for lemma in lemmas:
               	        result.append(lemma.name+"."+lemma.synset.pos)
	return result

#this method will return the noun of the given word
def getWordNounForm(word):
	resultSet = getWordForm(word)
	if not resultSet:#we cant find noun form for some word e.g., chronic, her
		return ''

#we perform two iteration to find the noun form, since some words return other forms than noun when called to get derivationally_related_form
#e.g., inflammatory (this return inflame, and inflame is verb, so we call once more to get the noun
	containsNoun = False
	for result in resultSet:
		if result.split('.')[1] == 'n':
			containsNoun = True

	if containsNoun:
		return getMostSimilarNoun(word, resultSet)
	else:
		resultSecondIteration = []
		for result in resultSet:
			for item in getWordForm(result.split('.')[0]):
				if item.split('.')[1] == 'n':
					resultSecondIteration.append(item)
	return getMostSimilarNoun(word, resultSecondIteration)

#resultsSet typically contain more than one term, we get the best term by calculating the levenshtein distance among the word and each term
def getMostSimilarNoun(word, resultSet):
	levenshteinDistance = []
	for result in resultSet:
		levenshteinDistance.append(levenshtein(result.split('.')[0], word))
	
	return resultSet[levenshteinDistance.index(min(levenshteinDistance))]


def getNounForAdverb(adverbLemma):
	result = []
	for lemma in adverbLemma.pertainyms():
		result.append(lemma.derivationally_related_forms())
	return result

def isAntonym(worda, wordb):
	wordasynsets = wn.synsets(worda[0], worda[1])
	wordbsynsets = wn.synsets(wordb[0], wordb[1])
	isAntonym = 0
	for syn in wordasynsets:
		for lemma in syn.lemmas:
			for antonym in lemma.antonyms():
				lst =[]
				lst.append(antonym.name)	
				lst.append(antonym.synset.pos)
				if antonym.name == wordb or isSynonym(lst, wordb):
					isAntonym = 1
	for syn in wordbsynsets:
                for lemma in syn.lemmas:
                        for antonym in lemma.antonyms():
				lst =[]
                                lst.append(antonym.name)
                                lst.append(antonym.synset.pos)
				if antonym.name == worda or isSynonym(lst, worda):
                                	isAntonym = 1
	return isAntonym

def isSynonym(worda, wordb):
	if worda[1] != wordb[1]:
                return 0

	if getWordNetSimilarity(worda, wordb) > 0.95:
		return 1

def getWordNetSimilarity(worda, wordb):
	wordasynsets = wn.synsets(worda[0], worda[1].lower())
        wordbsynsets = wn.synsets(wordb[0], wordb[1].lower())
        synsetnamea = [wn.synset(str(syns.name)) for syns in wordasynsets]
        synsetnameb = [wn.synset(str(syns.name)) for syns in wordbsynsets]
        brown_ic = wordnet_ic.ic('ic-brown.dat')

        lchmax = 3.6375861597263857
        resmax = 14.4655999131
        jcnmax = 1e+300
        similarities = []
        for sseta, ssetb in [(sseta,ssetb) for sseta in synsetnamea for ssetb in synsetnameb]:
                        wupsim = sseta.wup_similarity(ssetb)
                        lchsim = sseta.lch_similarity(ssetb)
                    #    ressim = sseta.res_similarity(ssetb, brown_ic)
                     #   jcnsim = sseta.jcn_similarity(ssetb, brown_ic)
                        linsim = sseta.lin_similarity(ssetb, brown_ic)
                        similarities.append(wupsim)
                        similarities.append(lchsim/lchmax)
                      #  similarities.append(ressim/resmax)
                       # similarities.append(jcnsim/jcnmax)
                        similarities.append(linsim)
        sim = max(similarities)
        if sim > 1:
        	sim = 1
	
	if sim < 0.6:
		return 0
	else:
		return sim	


def getKnowledgeBasedSimilarity(worda, wordb):
	if worda[1] != wordb[1]:
		return 0
        
	if isAntonym(worda, wordb):
		return -1

	if isSynonym(worda, wordb):
		return 1

	return getWordNetSimilarity(worda, wordb)


stopset = ['the', 'in', 'a', 'is', 'are', 'an']
data_file=sys.argv[1]
lines=open(data_file).readlines()
dictionary={}
with open("semantic-similarity-feature", "a") as f:
	lineno=1
        for line in lines:
		print 'processing line '+str(lineno)
		lineno=lineno+1
		synonymSimilarity = 0
                question=line.split('|')[7]
                answer=line.split('|')[13]

		question_tokens=[qtoken for qtoken in word_tokenize(question) if qtoken.lower() not in stopset]
                answer_tokens=[atoken for atoken in word_tokenize(answer) if atoken.lower() not in stopset]

		qnoun = [appForm for appForm in [getWordNounForm(com.strip()) for com in question_tokens]]
                qnoun[:] = [x for x in qnoun if x != '']
                anoun = [appForm for appForm in [getWordNounForm(com.strip()) for com in answer_tokens]]
                anoun[:] = [x for x in anoun if x != '']		

		qlength=len(qnoun)
		alength=len(anoun)
#		for compA, compB in [(compA, compB) for compA in qnoun for compB in anoun]:
#	                 compAElements = compA.split(".")
#                        compBElements = compB.split(".")
#                        if(isSynonym(compAElements, compBElements) and isSynonym(compBElements, compAElements)):#synonyms
#                                synonymSimilarity = synonymSimilarity + 1
#                                try:
#                                        qnoun.remove(compA)
#                                        anoun.remove(compB)
#                                except:
#                                        print 'abc'
                similarity = 0
                for compA in qnoun:
                        values = [0]
                        compAElements = compA.split(".")
                        for compB in anoun:
                                compBElements = compB.split(".")
				if compAElements[0]+':'+compBElements[0] in dictionary:
					sim = dictionary[compAElements[0]+':'+compBElements[0]]
					values.append(sim)
				else:											
                                	sim = getKnowledgeBasedSimilarity(compAElements, compBElements)
					dictionary[compAElements[0]+':'+compBElements[0]]=sim
                                	values.append(sim)
                        similarity = similarity + max(values)
                sim1= (similarity+synonymSimilarity)/qlength

		similarity = 0
                for compA in anoun:
                        values = [0]
                        compAElements = compA.split(".")
                        for compB in qnoun:
                                compBElements = compB.split(".")
				if compAElements[0]+':'+compBElements[0] in dictionary:
                                        sim = dictionary[compAElements[0]+':'+compBElements[0]]
                                        values.append(sim)
                                else:
                                        sim = getKnowledgeBasedSimilarity(compAElements, compBElements)
                                        dictionary[compAElements[0]+':'+compBElements[0]]=sim
                                        values.append(sim)
                        similarity = similarity + max(values)
                sim2= (similarity+synonymSimilarity)/alength
		f.write(str((sim1+sim2)/2)+'\n')
f.close()

