#This script will generate the pairwise similarity for each pair based on the 
#annotated types and using Jaccard coefficient
import os

def loadAnnotationMap():
	annotationmap = {}
	for file in os.listdir("../aggregated-annotations"):
		typeset = set()
		for line in open("../aggregated-annotations/"+file):
			lines = line.split('\t')
			typeannotation = lines[1].strip()
			if(typeannotation == 'ORGANIZATION'):
				typeannotation = 'LOCATION'
			typeset.add(typeannotation)
		filename = file.split('.')[0]
		annotationmap[filename] = typeset
	return annotationmap

def jaccardcoefficient(answerset1, answerset2):
	iSetIntersection = len(answerset1.intersection(answerset2))
	iSetUnion = len(answerset1.union(answerset2))
	dSetSimilarity = float(iSetIntersection) / float(iSetUnion)
	return dSetSimilarity



def printmaps(filename, dictelement):
	dictitmes = dictelement.items()
	printline = ""
	for dictItem in dictitmes:
		printline = printline + dictItem[0]+"\t"+str(dictItem[1]) + "\n"		
	#print printline	
	file = open(filename, 'w+')
	file.write(printline)

annotationmap = loadAnnotationMap()
print annotationmap
fileAveragesimilarity = {}
print 'annotationloaded'

for file in os.listdir("../QuestionCategoryPaired"):
	dPairAggregateSimilarity = 0.0
	iPairCount = 0
	
	for line in open("../QuestionCategoryPaired/"+file):
		dSimilarity = 0.0
		answers = line.split('|')
		answerpair1 = answers[0].strip()
		answerpair2 = answers[1].strip()
		answerset1 = set()
		answerset2 = set()
		if answerpair1 in annotationmap:
			answerset1 = annotationmap.get(answerpair1)
		if answerpair2 in annotationmap:	
			answerset2 = annotationmap.get(answerpair2)

		answer1setsize = len(answerset1)
		answer2setsize = len(answerset2)

		if (answer1setsize or answer2setsize):
			iPairCount = iPairCount + 1
			dSimilarity = jaccardcoefficient(answerset1, answerset2)
			dPairAggregateSimilarity = dPairAggregateSimilarity + dSimilarity	
		
	if iPairCount != 0:
		dAverageSimilarity = dPairAggregateSimilarity / float(iPairCount)
		#print file+'\t'+str(dAverageSimilarity)
	else:
		dAverageSimilarity = 0.0	
	fileAveragesimilarity[file] = dAverageSimilarity
	printmaps('../AnswerPairwiseSimilarity/PairWiseSimilarities', fileAveragesimilarity)

