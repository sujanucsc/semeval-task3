import xml.etree.ElementTree as ET
import os

for annotationFile in os.listdir('WATSON-annotations'):
	content=''
	tree = ET.parse('WATSON-annotations/'+annotationFile)
	root = tree.getroot()
	doc=root.find('doc')
	try:
		mentions=doc.findall('mentions')
	except AttributeError:
		print annotationFile
	for mention in mentions[0].findall('mention'):
		etype=mention.attrib['etype']
		content+=mention.text+"\t"+etype+"\n"

	f=open('WATSON-annotations-processed/'+annotationFile.replace('xml', 'txt'), 'w')
	f.write(content)
	f.close()

