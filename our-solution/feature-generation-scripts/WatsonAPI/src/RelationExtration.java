import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.message.BasicNameValuePair;

import com.ibm.json.java.JSONArray;
import com.ibm.json.java.JSONObject;

public class RelationExtration {

	private static String  serviceName = "relationship_extraction";
	
	// If running locally complete the variables below with the information in VCAP_SERVICES
	private static String baseURL = "https://gateway.watsonplatform.net/laser/service/api/v1/sire/0cd6cfc1-057f-494b-9908-efce7d4b7aef";
	private static String username = "21e3e3f5-4bee-4f30-bbcf-c1e2a109d60e";
	private static String password = "omlN3jZPW2LE";
	
	public static void main(String[] args) throws ClientProtocolException,
			IOException {

		processVCAP_Services();
		
		Scanner scan = new Scanner(new File("train-data-set"));
		Map<String, String> texts = new HashMap<String, String>();
		while(scan.hasNextLine()){
			String line = scan.nextLine();
			String[] components = line.split("\\|");
			
			String qSubject = components[6];
			String qBody = components[7];
			String aSubject=components[12];
			String aBody=components[13];
			
			if(!qSubject.endsWith(".")){
				qSubject+=".";
			}
			
			if(!aSubject.endsWith(".")){
				aSubject+=".";
			}
			
			String question=qSubject+" "+qBody;
			String answer=aSubject+" "+aBody;
			
			String qID = components[0];
			String aID = components[8];
			
			texts.put(qID, question);
			texts.put(aID, answer);
		}
		
		try {
			Executor executor = Executor.newInstance().auth(username, password);
			URI serviceURI = new URI(baseURL).normalize();
			String auth = username + ":" + password;
			
			for (Map.Entry<String, String> entry:texts.entrySet()){
				List<NameValuePair> qparams = new ArrayList<NameValuePair>();
				
				File f = new File("results/"+entry.getKey()+".xml");
				if(f.exists()){
					System.out.println(f.getName());
					continue;
				}
					
				qparams.add(new BasicNameValuePair("sid", "ie-en-news"));
				qparams.add(new BasicNameValuePair("rt", "xml"));
				qparams.add(new BasicNameValuePair("txt", entry.getValue()));
				
				byte[] responseB = executor.execute(Request.Post(serviceURI).addHeader("Authorization", "Basic " + Base64.encodeBase64String(auth.getBytes()))
						.bodyString(URLEncodedUtils.format(qparams, "utf-8"), ContentType.APPLICATION_FORM_URLENCODED)).returnContent().asBytes();

				String response = new String(responseB, "UTF-8");
				writeToFile(entry.getKey(), response);
				Thread.sleep(1000);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void writeToFile(String key, String response) throws IOException {
		FileWriter fw = new FileWriter("results/"+key+".xml");
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(response);
		bw.close();
		
	}

	private static void processVCAP_Services() {
        JSONObject sysEnv = getVcapServices();
        if (sysEnv == null) return;
        
        if (sysEnv.containsKey(serviceName)) {
			JSONArray services = (JSONArray)sysEnv.get(serviceName);
			JSONObject service = (JSONObject)services.get(0);
			JSONObject credentials = (JSONObject)service.get("credentials");
			baseURL = (String)credentials.get("url");
			username = (String)credentials.get("username");
			password = (String)credentials.get("password");
    	} else {
        	System.out.println(serviceName + " is not available in VCAP_SERVICES, "
        			+ "please bind the service to your application");
        }
    }
	
	 private static JSONObject getVcapServices() {
	        String envServices = System.getenv("VCAP_SERVICES");
	        if (envServices == null) return null;
	        JSONObject sysEnv = null;
	        try {
	        	 sysEnv = JSONObject.parse(envServices);
	        } catch (IOException e) {
	        	e.printStackTrace();
	        }
	        return sysEnv;
	    }


}
