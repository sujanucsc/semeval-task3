#!/bin/sh

previousquestion='Q'
while read line
do
	
	question=`echo $line | cut -d'|' -f8`
	answer=`echo $line | cut -d'|' -f14`
	commentid=`echo $line | cut -d'|' -f9`
	questionid=`echo $line | cut -d'|' -f1`
	
	if [ $questionid != $previousquestion ] ; then
		questionAnnotation=`curl http://spotlight.dbpedia.org/rest/annotate --data-urlencode "text=$question"`
		echo $questionAnnotation > annotations/$questionid
		previousquestion=$questionid
	fi
	
	answerAnnotation=`curl http://spotlight.dbpedia.org/rest/annotate --data-urlencode "text=$answer"`

	echo $answerAnnotation > annotations/$commentid
	sleep 5
done < "../train-data-set"


