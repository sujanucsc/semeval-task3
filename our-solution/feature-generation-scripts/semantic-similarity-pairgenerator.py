#For each question, for each category(good and bad) create the all possible list of answer pairs
#input folder "QuestionCategories" For each question, for each category list the possible answers
#output folder "QuestionCategoryPaired" For each question, for each category list the possible answer pairs 
import os

def createpairs(lstoflines,filename):

	printlines=""	
	for i in range(len(lstoflines)):
		j = i+1
		for j in range(j,len(lstoflines)):
			newline = lstoflines[i].strip()+"|"+lstoflines[j].strip()
			printlines = printlines + newline + "\n"
	file = open("../QuestionCategoryPaired/"+filename, 'w+')
	file.write(printlines)

for file in os.listdir("../QuestionCategories"):
	print file
	lstoflines = list()
	for line in open("../QuestionCategories/"+file):
		lstoflines.append(line)

	createpairs(lstoflines,file)
