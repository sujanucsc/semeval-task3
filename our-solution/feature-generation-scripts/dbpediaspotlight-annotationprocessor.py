import xml.etree.ElementTree as ET
import os

def xmlprocessor(filename):
	print filename
	tree = ET.parse(filename)
	root = tree.getroot()
	
	label = ""
	for resources in root:		
		for resource in resources.findall('Resource'):
			surfaceform = resource.attrib['surfaceForm']
			url = resource.attrib['URI']
			linelabel = surfaceform+'\t'+url+'\n'
			label = label+linelabel
		label = label[:-1]
	return label

def writeFile(filename, label):
	file = open(filename, 'w+')
	file.write(label)

for file in os.listdir("../dbpediaannotations"):
	label = xmlprocessor("../dbpediaannotations/"+file)
	writeFile("../dbpediaannotationsprocessed/"+file,label)	

#label = xmlprocessor("../dbpediaannotations/"+"Q22_C8")
#print label
#writeFile("../dbpediaannotationsprocessed/"+"Q22_C8",label)


