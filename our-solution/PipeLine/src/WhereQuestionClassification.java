import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


public class WhereQuestionClassification {
	
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Map<String, String> filteredAnswers = filterWhereQuestionsAndAnswerIDs();

		StringBuffer predictions = new StringBuffer();
		for(Map.Entry<String, String> filteredAnswer : filteredAnswers.entrySet()){
			String predictedAnnotation = "Bad";
			Scanner scan = new Scanner(new File("../feature-generation-scripts/aggregated-annotations/"+filteredAnswer.getKey()+".txt"));
			
			String commentID = filteredAnswer.getKey();
			
			while(scan.hasNextLine()){
				String annotation = scan.nextLine();
				
				if(annotation.contains("LOCATION") || annotation.contains("ORGANIZATION") || annotation.contains("GPE")){
					predictedAnnotation = "Good";
				}
			}
			
			predictions.append(commentID+"\t"+predictedAnnotation+"\n");
		}

//		Util.writeToFile("../predictions-for-where-questions", predictions.toString());
	}
	
	private static Map<String, String> filterWhereQuestionsAndAnswerIDs() throws FileNotFoundException{
		Scanner scan = new Scanner(new File("../train-data-set"));
		
		Set<String> temp = new HashSet<String>();
		Map<String, String> filteredAnswers = new HashMap<String, String>();
		while(scan.hasNextLine()){
			String line = scan.nextLine();
			String components[] = line.split("\\|");
			
			String qSub = components[6];
			String qBody = components[7];
			
			if(qSub.contains(" where ") || qBody.contains(" where ")){
				temp.add(components[0]);
				String aID = components[8];
				filteredAnswers.put(aID, components[13]);
			}
		}
		
		System.out.println(temp.size());
		return filteredAnswers;
	}
	
	

}
