import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class AnnotationAggregation {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Map<String, String> dialog = new HashMap<String, String>();
		Scanner scan = new Scanner(new File("../predictions-for-dialogue-comments"));
		
		while(scan.hasNextLine()){
			String line[] = scan.nextLine().split("\t");
			dialog.put(line[0], line[1]);
		}
		
		
		Map<String, String> where = new HashMap<String, String>();
		scan = new Scanner(new File("../predictions-for-where-questions"));
		
		while(scan.hasNextLine()){
			String line[] = scan.nextLine().split("\t");
			where.put(line[0], line[1]);
		}
		
		StringBuffer buf = new StringBuffer();
		for(Map.Entry<String, String> entry : where.entrySet()){
			if(dialog.containsKey(entry.getKey()) && dialog.get(entry.getKey()).equals("Dialogue")){
				buf.append(entry.getKey()+"\tDialogue\n");
			}else{
				buf.append(entry.getKey()+"\t"+entry.getValue()+"\n");
			}
		}
		
		Util.writeToFile("../our-prediction", buf.toString());
	}

}
