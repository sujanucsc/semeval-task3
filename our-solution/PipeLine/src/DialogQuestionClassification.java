import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class DialogQuestionClassification {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Scanner scan = new Scanner(new File("../train-data-set"));
		
		StringBuffer dialogs = new StringBuffer();
		while(scan.hasNextLine()){
			String line = scan.nextLine();
			String components[] = line.split("\\|");
			
			String qUser = components[3].trim();
			String aUser = components[9].trim();
			
			if(qUser.equals(aUser)){
				dialogs.append(components[8]+"\t"+"Dialogue\n");
			}else{
				dialogs.append(components[8]+"\t"+"Unknown\n");
			}
		}

		Util.writeToFile("../predictions-for-dialogue-comments", dialogs.toString());
	}

}
