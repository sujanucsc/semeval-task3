import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;


public class Util {

	public static void writeToFile(String fileName, String content) throws IOException{
		FileWriter fw = new FileWriter(fileName);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(content);
		bw.close();
	}

}
