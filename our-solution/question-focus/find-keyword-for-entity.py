import os
from nltk.tokenize import RegexpTokenizer
import nltk
from nltk.corpus import stopwords
from collections import OrderedDict
from nltk.tokenize import sent_tokenize
from textblob import TextBlob
from corenlp import StanfordCoreNLP
from collections import Counter

tokenizer=RegexpTokenizer(r'\w+')
stemmer=nltk.PorterStemmer()
token_ordered_file='entropy-stemmed'

tokens_and_values=dict()
for line in open(token_ordered_file).readlines():
	tokens_and_values[line.split('\t')[0]]=line.split('\t')[1].strip()

questions=open('questions').readlines()
top_words=[]
for question in questions:
	qID=question.split('\t')[0]
	question=question.split('\t')[1]
	stemmed_tokens=[(token, stemmer.stem(token)) for token in tokenizer.tokenize(question)]
	stemmed_with_values=dict()
	
	for tup in stemmed_tokens:
		word=tup[0]
		stemmed_word=tup[1]	
                try:
                        stemmed_with_values[word]=float(tokens_and_values[stemmed_word])
                except KeyError:
                        stemmed_with_values[word]=0
        sorted_dict=OrderedDict(sorted(stemmed_with_values.items(), key=lambda t: float(t[1])))
	
	s=''
	for term in list(reversed(sorted_dict)):
		s+=term+', '
	print qID+'\t'+s

