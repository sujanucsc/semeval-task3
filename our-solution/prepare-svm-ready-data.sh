#!/bin/sh


#rm *-feature #delete all existing feature files
rm train-data-svm-ready


#`python class-label.py train-data-set` #generate class labels
#for f in *-feature.py
#do
#	`python $f train-data-set` #run all python scripts
#done


#generate the file compatible with libsvm input format
line_no=0
p='p'
while read line
do
        line_no=$((line_no + 1)) #Iam not sure why I have to do this, just $line_no+1 should work
	feature_no=1
	trainline=$line'\t'
        for f in *-feature
        do
               	temp="`sed -n $line_no$p $f`"
		trainline=$trainline$feature_no':'$temp'\t'
		feature_no=$((feature_no + 1))
        done
	echo $trainline >> train-data-svm-ready
done < "class-label"
