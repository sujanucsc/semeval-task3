#!/usr/bin/perl
#
#  Author: Preslav Nakov
#  
#  Description: Baseline system for subtask A of SemEval-2015 Task 3.
#
#  Version: 1.0
#
#  Last modified: October 13, 2014
#
#
#  Use:
#     SemEval2015-task3-baseline-subtaskA.pl <TRAIN_FILE> <TEST_INPUT_FILE> <PREDICTIONS_FILE>
#
#  Example use:
#     SemEval2015-task3-baseline-subtaskA.pl ../sample_data/CQA-QL-train.xml ../sample_data/CQA-QL-devel-input.xml CQA-QL-devel-predicted-subtaskA.txt
#
#  Description:
#     This is an implementation of a simple majority class baseline.
#     First, the most frequent label from training is found.
#     Then, this label is predicted for all examples in the test input.
#
#


use warnings;
use strict;
use utf8;


################
###   MAIN   ###
################

### 1. Check the parameters
die "Usage: $0 <TRAIN_FILE> <TEST_INPUT_FILE> <PREDICTIONS_FILE>\n" if ($#ARGV != 2);
my $TRAIN_FILE       = $ARGV[0];
my $TEST_INPUT_FILE  = $ARGV[1];
my $PREDICTIONS_FILE = $ARGV[2];

### 2. Train the system
my $mostFrequentLabel = &getMostFrequentLabel($TRAIN_FILE);

### 3. Generate the predictions
open TEST_INPUT, $TEST_INPUT_FILE or die "Error opening $TEST_INPUT_FILE!";
binmode(TEST_INPUT, ":utf8");
open PREDICTIONS, '>' . $PREDICTIONS_FILE or die "Error opening $PREDICTIONS_FILE!";
binmode(PREDICTIONS, ":utf8");
while (<TEST_INPUT>) {
	if (/<Comment CID=\"(Q[0-9]+\_C[0-9]+)\" CUSERID=\"U[0-9]+\" CGOLD=\"\?\"[^\>]*>/) {
		my $cid = $1;
		print PREDICTIONS "$cid\t$mostFrequentLabel\n";
	}
}
close TEST_INPUT or die;


################
###   SUBS   ###
################

sub getMostFrequentLabel() {
	my $fname = shift;
	my %goldLabels = ();

	### 1. Collect the statictics
	open INPUT, $fname or die "Error opening $fname!";
	binmode(INPUT, ":utf8");
	while (<INPUT>) {
		###  <Comment CID="Q1_C3" CUSERID="U1" CGOLD="Dialogue" CGOLD_YN="Not Applicable">
		if (/<Comment CID=\"Q[0-9]+\_C[0-9]+\" CUSERID=\"U[0-9]+\" CGOLD=\"([^\"]+)\"[^\>]*>/) {
			my $gold = $1;
			$goldLabels{$gold}++;
		}
	}
	close INPUT or die;

	### 2. Find the most frequent label
	my @sorted = sort {$goldLabels{$b} <=> $goldLabels{$a}} keys %goldLabels;

	### 3. Return it as a result
	return $sorted[0];
}
