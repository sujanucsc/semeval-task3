import xml.etree.ElementTree as ET

tree = ET.parse('data/SemEval2015-Task3-English-data/datasets/CQA-QL-train.xml')
root = tree.getroot()

with open("our-solution/train-data-set", "a") as f:
	for question in root:
		qid = question.attrib['QID']
		qcategory = question.attrib['QCATEGORY']
		qdate = question.attrib['QDATE']
		quser = question.attrib['QUSERID']
		qtype = question.attrib['QTYPE']
		qyn = question.attrib['QGOLD_YN']

		qsubject = question.find('QSubject').text.replace('\n','').replace('\r','')
		qbody = question.find('QBody').text.replace('\n','').replace('\r','')

		for comment in question.findall('Comment'):
			cid = comment.attrib['CID']
			cuser = comment.attrib['CUSERID']
			cgold = comment.attrib['CGOLD']
			cyn = comment.attrib['CGOLD_YN']
	
			csubject = comment.find('CSubject').text.replace('\n','').replace('\r','')
			cbody = comment.find('CBody').text.replace('\n', '').replace('\r','')
			
			f.write(qid+'|'+qcategory+'|'+qdate+'|'+quser+'|'+qtype+'|'+qyn+'|'+qsubject+'|'+qbody+'|'+cid+'|'+cuser+'|'+cgold+'|'+cyn+'|'+csubject+'|'+cbody+'\n')

f.close()

